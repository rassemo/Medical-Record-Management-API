const Intervention = require("../models/intervention.model");
const Patient = require("../models/patient.model");
const access = require("../middlewares/authorization");




module.exports = {

    // get all intervention admin only
    getAll: (req, res, next) => {

        access.authorize(req,["admin"])

        Intervention.find({}).then((interventions) => {
            res.statusCode = 200;
            res.setHeader("content-Type", "application/json");
            res.json(interventions);

        }, err => next(err)
        )
            .catch(err => next(serr));
    },

    // get all the patient of one connected doctor
    getAllByDoctor:(req, res, next) => {

        access.authorize(req,["admin","medOp"])

        var ssn = req.user.ssn
        if(req.user.role == "admin") ssn = req.body.ssnAgent
        
        Intervention.find({ssnAgent:ssn})
            .then((interventions) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(interventions);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    // get all the interventions of one patient
   // --- add router ---
    getOnePatientForDoctor:(req, res, next) => {

        access.authorize(req,["medCons","medOp"])
        
        Intervention.find({ssnPatient:req.params.ssnp})
            .then((interventions) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(interventions);

            }, err => next(err)
            )
            .catch(err => next(err));

    },    

    // get all the interventions made by a connected agent 
    //or a specific for the admin case
    getAllByAgent:(req, res, next) => {

        access.authorize(req,["admin","medOp"])

        var ssn = req.user.ssn
        if(req.user.role == "admin") ssn = req.body.ssnAgent
        
        Intervention.find({ssnAgent:ssn})
            .then((interventions) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(interventions);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    //get all the interventions of a single connected patient
    getAllByPatient:(req, res, next) => {

        access.authorize(req,["patient"])
        
        Intervention.find({ssnPatient:req.user.ssn})
            .then((interventions) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(interventions);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    //get all the interventions of a single patient from its ssn
    // made  by a connected doctor
    getAllByPatientForDoctor:(req, res, next) => {

        access.authorize(req,["medOp"])
        
        Intervention.find({ssnAgent:req.user.ssn,ssnPatient:req.params.ssnp})
            .then((interventions) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(interventions);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    // get all the interventions of a certain type made by a connected doctor
    getAllByTypeForDoctor:(req, res, next) => {

        access.authorize(req,["medOp"])
        
        Intervention.find({ssnAgent:req.user.ssn,interventionType:req.params.type})
            .then((interventions) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(interventions);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    // get all the interventions of a certain type of a connected patient
    getAllByTypeForPatient:(req, res, next) => {

        access.authorize(req,["patient"])
        
        Intervention.find({ssnPatient:req.user.ssn,interventionType:req.params.type})
            .then((interventions) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(interventions);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    // get all the interventions of a certain type of a given patient for a connected doctor
    getAllByTypeAndPatientForDoctor:(req, res, next) => {

        access.authorize(req,["medCons","medOp"])
        
        Intervention.find({
                ssnPatient:req.params.ssnp,
                analysisType:req.params.type
            })
            .then((interventions) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(interventions);

            }, err => next(err)
            )
            .catch(err => next(err));
    },
    
    // add one new intervention
    addOne: (req, res, next) => {

        access.authorize(req,["medOp"])

        Patient.find({ssn:req.body.ssnPatient})
            .then((patients) => {
                if(patients.length > 0){
                    req.body.ssnAgent = req.user.ssn
                    Intervention.create(req.body)
                    .then((intervention) => {
                        res.statusCode = 200;
                        res.setHeader("content-Type", "application/json");
                        res.json(intervention);
                    }, err => next(err)
                    )
                    .catch(err => next(err));
                }
                else{
                    throw new Error("can't save ressource : patient not found!")
                }
            }, err => next(err)
            )
            .catch(err => next(err));

    },

    // delete one intervention based on id admin only
    deleteOne: (req, res, next) => {

        access.authorize(req,["admin"])
        
        Intervention.findByIdAndDelete(req.params.id)
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(resp)

            }, err => next(err)
            )
            .catch(err => next(err));

    },

    // delete one intervention made by the connected agent
    deleteOneSelf:(req, res, next) => {

        access.authorize(req,["medOp"])

        Intervention.findOneAndDelete({_id:req.params.id,ssnAgent:req.user.ssn})
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(resp)

            }, err => next(err)
            )
            .catch(err => next(err));

    },

    // get one intervention based on id admin only
    getOne: (req, res, next) => {

        access.authorize(req,["admin"])

        Intervention.findById(req.params.id)
            .then((intervention) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(intervention);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    // update one of the interventions made by a connected doctor
    updateOne: (req, res, next) => {

        access.authorize(req,["medOp"])
        Intervention.findOneAndUpdate({_id:req.params.id,ssnAgent:req.user.ssn}, { $set: req.body }, { new: true })
        .then((intervention) => {
            res.statusCode = 200;
            res.setHeader("content-Type", "application/json");
            res.json(intervention);

        }, err => next(err)
        )
        .catch(err => next(err));
    },

}