const Consultation = require("../models/consultation.model");
const Patient = require("../models/patient.model");
const access = require("../middlewares/authorization");

module.exports = {

    getAll: (req, res, next) => {

        access.authorize(req,["admin"])

        Consultation.find({}).then((consultations) => {
            res.statusCode = 200;
            res.setHeader("content-Type", "application/json");
            res.json(consultations);

        }, err => next(err)
        )
        .catch(err => next(err));
    },

    getOne: (req, res, next) => {

        access.authorize(req,["admin"])
        
        Consultation.findById(req.params.id)
            .then((consultation) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(consultation);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    getAllByDoctor:(req, res, next) => {

        access.authorize(req,["admin","medCons"])

        var ssn = req.user.ssn
        if(req.user.role == "admin") ssn = req.body.ssnAgent
        
        Consultation.find({ssnAgent:ssn})
            .then((consultations) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(consultations);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    getAllByPatient:(req, res, next) => {

        access.authorize(req,["patient"])
        
        Consultation.find({ssnPatient:req.user.ssn})
            .then((consultations) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(consultations);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    getAllByPatientForDoctor:(req, res, next) => {

        access.authorize(req,["medCons"])
        
        Consultation.find({ssnAgent:req.user.ssn,ssnPatient:req.params.ssnp})
            .then((consultations) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(consultations);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    getOnePatientForDoctor:(req, res, next) => {

        access.authorize(req,["medCons","medOp"])
        
        Consultation.find({ssnPatient:req.params.ssnp})
            .then((consultations) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(consultations);

            }, err => next(err)
            )
            .catch(err => next(err));

    },

    getAllByTypeForDoctor:(req, res, next) => {

        access.authorize(req,["medCons"])
        
        Consultation.find({ssnAgent:req.user.ssn,consultationType:req.params.type})
            .then((consultations) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(consultations);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    getAllByTypeForPatient:(req, res, next) => {

        access.authorize(req,["patient"])
        
        Consultation.find({ssnPatient:req.user.ssn,consultationType:req.params.type})
            .then((consultations) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(consultations);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    getAllByTypeAndPatientForDoctor:(req, res, next) => {

        access.authorize(req,["medCons","medOp"])
        
        Consultation.find({
                
                ssnPatient:req.params.ssnp,
                consultationType:req.params.type
            })
            .then((consultations) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(consultations);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    addOne:(req, res, next) => {

        access.authorize(req,["medCons"])

        Patient.find({ssn:req.body.ssnPatient})
            .then((patients) => {
                if(patients.length > 0){
                    req.body.ssnAgent = req.user.ssn
                    Consultation.create(req.body)
                    .then((consultation) => {
                        res.statusCode = 200;
                        res.setHeader("content-Type", "application/json");
                        res.json(consultation);
                        // gestion des erreures via la focntion next
                    }, err => next(err))
                    .catch(err => next(err));
                }
                else{
                    throw new Error("can't save ressource : patient not found!")
                }
            }, err => next(err)
            )
            .catch(err => next(err));
    },

    deleteOne: (req, res, next) => {

        access.authorize(req,["admin"])

        Consultation.findByIdAndDelete(req.params.id)
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(resp)

            }, err => next(err)
            )
            .catch(err => next(err));

    },

    deleteOneSelf:(req, res, next) => {

        access.authorize(req,["medCons"])

        Consultation.findOneAndDelete({_id:req.params.id,ssnAgent:req.user.ssn})
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(resp)

            }, err => next(err)
            )
            .catch(err => next(err));

    },

    updateOne: (req, res, next) => {

        access.authorize(req,["medCons"])

        Consultation.findOneAndUpdate({_id:req.params.id,ssnAgent:req.user.ssn}, { $set: req.body }, { new: true })
            .then((consultation) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(consultation);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

 

}