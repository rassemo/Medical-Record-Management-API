const mongoose = require("mongoose");
const Radiographie = require("../models/radiographie.model");
const access = require("../middlewares/authorization");

module.exports = {

    // get all radiographies admin only
    getAll: (req, res, next) => {

        access.authorize(req,["admin"])

        Radiographie.find({})
        .then((radiographies) => {
            res.statusCode = 200;
            res.setHeader("content-Type", "application/json");
            res.json(radiographies);

        }, err => next(err)
        )
            .catch(err => next(err));
    },

    // get all the radiography of one patient
    getOnePatientForDoctor:(req, res, next) => {

        access.authorize(req,["medCons","medOp"])
        
        Radiographie.find({ssnPatient:req.params.ssnp})
            .then((radiographies) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(radiographies);

            }, err => next(err)
            )
            .catch(err => next(err));

    },

    
    getAllByDoctor:(req, res, next) => {

        access.authorize(req,["admin","medRad"])

        var ssn = req.user.ssn
        if(req.user.role == "admin") ssn = req.body.ssnAgent
        
        Radiographie.find({ssnAgent:ssn})
            .then((radiographies) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(radiographies);

            }, err => next(err)
            )
            .catch(err => next(err));
    },


    // get all the radiography made by a connected agent 
    //or a specific for the admin case
    getAllByAgent:(req, res, next) => {

        access.authorize(req,["admin","medRad"])

        var ssn = req.user.ssn
        if(req.user.role == "admin") ssn = req.body.ssnAgent
        
        Radiographie.find({ssnAgent:ssn})
            .then((radiographies) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(radiographies);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    //get all the radiography of a single connected patient
    getAllByPatient:(req, res, next) => {

        access.authorize(req,["patient"])
        
        Radiographie.find({ssnPatient:req.user.ssn})
            .then((radiographies) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(radiographies);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    //get all the radiography of a single patient from its ssn
    // by a connected doctor
    getAllByPatientForDoctor:(req, res, next) => {

        access.authorize(req,["medRad"])
        
        Radiographie.find({ssnAgent:req.user.ssn,ssnPatient:req.params.ssnp})
            .then((radiographies) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(radiographies);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    // get all the radiographies of a certain type made by a connected doctor
    getAllByTypeForDoctor:(req, res, next) => {

        access.authorize(req,["medRad"])
        
        Radiographie.find({ssnAgent:req.user.ssn,radioType:req.params.type})
            .then((radiographies) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(radiographies);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    // get all the radiographies of a certain type of a connected patient
    getAllByTypeForPatient:(req, res, next) => {

        access.authorize(req,["patient"])
        
        Radiographie.find({ssnPatient:req.user.ssn,radioType:req.params.type})
            .then((radiographies) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(radiographies);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    // get all the radiographies of a certain type of a given patient for a connected doctor
    getAllByTypeAndPatientForDoctor:(req, res, next) => {

        access.authorize(req,["medCons","medOp"])
        
        Radiographie.find({
                ssnPatient:req.params.ssnp,
                radioType:req.params.type
            })
            .then((radiographies) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(radiographies);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    // add a new radiography
    addOne: (req, res, next) => {

        access.authorize(req,["medRad"])

        const radiographie = new Radiographie(
            {
                _id: new mongoose.Types.ObjectId(),
                ssnPatient: req.body.ssnPatient,
                ssnAgent: req.user.ssn,
                date:req.body.date,
                title: req.body.title,
                radioType: req.body.radioType,
                path: req.file.path.replace("\\", "/")

            });
            radiographie.save()
            // then s'execute si tout va bien
            .then((radiographie) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(radiographie);
                // gestion des erreures via la focntion next
            }, err => next(err)
            )
            .catch(err => next(err));

    },

    // delete one radiography by id admin only
    deleteOne: (req, res, next) => {

        access.authorize(req,["admin"])

        Radiographie.findByIdAndDelete(req.params.id)
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(resp)

            }, err => next(err)
            )
            .catch(err => next(err));

    },
     
    // delete one radiography made by a connected doctor
    deleteOneSelf:(req, res, next) => {

        access.authorize(req,["medRad"])

        Radiographie.findOneAndDelete({_id:req.params.id,ssnAgent:req.user.ssn})
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(resp)

            }, err => next(err)
            )
            .catch(err => next(err));

    },

    // get one radiography by id admin only
    getOne: (req, res, next) => {

        access.authorize(req,["admin"])

        Radiographie.findById(req.params.id)
            .then((radiographie) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(radiographie);

            }, err => next(err)
            )
            .catch(err => next(err));
    },

    //update one radiography made by the connected doctor
    updateOne: (req, res, next) => {

        access.authorize(req,["medRad"])
        
        // le paramettre new force la fonction a renvoyer la nouvelle version d'radiographie'
        Radiographie.findOneAndUpdate({ssnAgent:req.user.ssn, _id:req.params.id}, {
             ssnPatient: req.body.ssnPatient,
             ssnAgent: req.user.ssn,
             date:req.body.date,
             title: req.body.title,
             radioType: req.body.radioType,
             path:req.file.path.replace("\\", "/")
             
         }, { new: true })
            .then((radiographie) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(radiographie);

            }, err => next(err)
            )
            .catch(err => next(err));

    },

}