const express = require('express');
const controler = require("../controller/agent.controller")
const router = express.Router();
const passport = require('passport');

router.route('/')
    .all((req, res, next) => {
        //fait les premier traitmeent à priorie ici on considère qu'on va dire 200 par défaut
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();
    })
    .get(passport.authenticate('jwt', { session: false }), controler.getAll)
    .post(passport.authenticate('jwt', { session: false }), controler.addOne)

router.route('/self/info')
    .all((req, res, next) => {
        //fait les premier traitmeent à priorie ici on considère qu'on va dire 200 par défaut
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();
    })
    .get(passport.authenticate('jwt', { session: false }), controler.getSelf)


// signup & login
router.route('/signup').post(controler.addOne);

router.route('/login')
    .post(controler.login)
    .get((req,res)=>{res.render('login-agent')})

router.route("/:ssn")
    .all((req, res, next) => {
        //fait les premier traitmeent à priorie ici on considère qu'on va dire 200 par défaut
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    .get(passport.authenticate('jwt', { session: false }), controler.getOne)
    .delete(passport.authenticate('jwt', { session: false }), controler.deleteOne)
    .put(passport.authenticate('jwt', { session: false }), controler.updateOne)


module.exports = router;

