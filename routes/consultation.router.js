const express = require('express');
const controler = require("../controller/consultation.controller")
const router = express.Router();

/*
The resource must be accessible by :
    - Admin user
    - concerned doctor
    - concerned patient
*/

// only for admin
router.route('/')
    .all((req, res, next) => {
        //fait les premier traitmeent à priorie ici on considère qu'on va dire 200 par défaut
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    .get(controler.getAll);

router.route("/doctor-self")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    // get all consultations made by a specific doctor
    .get(controler.getAllByDoctor)
    // add consulattion : specific doctor
    .post(controler.addOne)

// get all consultations made by a specific doctor with a specific patient
router.route("/doctor-self/patient/:ssnp")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    .get(controler.getAllByPatientForDoctor);

router.route("/doctor-self/ressource/:id")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    // update consulattion : specific doctor
    .post(controler.updateOne)
    // delete consulattion : specific doctor
    .delete(controler.deleteOneSelf);


// search a consultation by type : for specific doctor
router.route("/doctor-self/search/:type")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    .get(controler.getAllByTypeForDoctor);

// search a consultation by type : specific doctor + specifc patient
router.route("/doctor-self/search/:type/patient/:ssnp")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    .get(controler.getAllByTypeAndPatientForDoctor);

router.route("/patient-self")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    // get all consultations of a specific patient
    .get(controler.getAllByPatient)

router.route("/patient-self/search/:type")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    // get a type of consultation for a specific patient
    .get(controler.getAllByTypeForPatient)

// get one patient consultation for a doctor
router.route("/doctor/patient/:ssnp")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    .get(controler.getOnePatientForDoctor)

// only for admin
router.route("/:id")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    .get(controler.getOne)
    .delete(controler.deleteOne);

module.exports = router;

