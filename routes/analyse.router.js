const express = require('express');
const controler = require("../controller/analyse.controller")
const router = express.Router();

// bibliothéque permettant de gérer l'envois de fichiers
const multer = require('multer');
// filtre type de fichier
const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'application/pdf') {
        //accepter le ficher
        cb(null, true);
    }
    else {
        // rejeter un fichier
        cb(new Error('fichier non conforme'), false);

    }
};
// constantes de stockage
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        // cette fonction call back gère les erreure 
        // et prend le chemain de stockage de données
        cb(null, './analyses');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    },
});
// Multer package init
// upload repository config 
const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 45,
    },
    fileFilter: fileFilter
});


/*
The resource must be accessible by :
    - Admin user
    - concerned doctor
    - concerned patient
*/

// route regroupant les fonctions ne prenant pas de parametre 
router.route('/')
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();
    })
    // renvois de l'ensemble des analyses
    .get(controler.getAll)

router.route("/doctor-self")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    // get all consultations made by a specific doctor
    .get(controler.getAllByDoctor)
    // add consulattion : specific doctor
    .post(upload.single('analyse'),controler.addOne)

// get all consultations made by a specific doctor with a specific patient
router.route("/doctor-self/patient/:ssnp")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    .get(controler.getAllByPatientForDoctor)

router.route("/doctor-self/ressource/:id")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    // update consulattion : specific doctor
    .post(upload.single('analyse'), controler.updateOne)
    // delete consulattion : specific doctor
    .delete(controler.deleteOneSelf)

// search a consultation by type : for specific doctor
router.route("/doctor-self/search/:type")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    .get(controler.getAllByTypeForDoctor)

// search a consultation by type : specific doctor + specifc patient
router.route("/doctor-self/search/:type/patient/:ssnp")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    .get(controler.getAllByTypeAndPatientForDoctor)

router.route("/patient-self")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    // get all consultations of a specific patient
    .get(controler.getAllByPatient)

router.route("/patient-self/search/:type")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    // get a type of consultation for a specific patient
    .get(controler.getAllByTypeForPatient)

// get one patient consultation for a doctor
router.route("/doctor/patient/:ssnp")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    .get(controler.getOnePatientForDoctor);
    
// route regroupant l'ensemble des fonctions ayant besoin d'un parametre id
router.route("/:id")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    .get(controler.getOne)
    .delete(controler.deleteOne);


module.exports = router;

