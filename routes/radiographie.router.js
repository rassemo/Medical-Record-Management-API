const express = require('express');
const controler = require("../controller/radiographie.controller")
const router = express.Router();
// bibliothéque permettant de gérer l'envois de fichiers
const multer = require('multer');
// filtre type de fichier
const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg' || file.mimetype === 'image/png') {
        //accepter le ficher
        cb(null, true);
    }
    else {
        // rejeter un fichier
        cb(new Error('fichier non conforme'), false);

    }
};
// constantes de stockage
const storage = multer.diskStorage({
    destination: function (req, file, cb) {

        // cette fonction call back gère les erreure 
        // et prend le chemain de stockage de données
        cb(null, './radiographies');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    },
});
// Multer package init
// upload repository config 
// in app.js we mad that folder static (public proof)
const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 35,
    },
    fileFilter: fileFilter
});


router.route('/')
    .all((req, res, next) => {
        //fait les premier traitmeent à priorie ici on considère qu'on va dire 200 par défaut
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    .get(controler.getAll)

router.route("/doctor-self")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    // get all consultations made by a specific doctor
    .get(controler.getAllByDoctor)
    // add consulattion : specific doctor
    .post(upload.single('radiographie'), controler.addOne)

// get all consultations made by a specific doctor with a specific patient
router.route("/doctor-self/patient/:ssnp")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    .get(controler.getAllByPatientForDoctor)

router.route("/doctor-self/ressource/:id")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    // update consulattion : specific doctor
    .post(upload.single('radiographie'), controler.updateOne)
    // delete consulattion : specific doctor
    .delete(controler.deleteOneSelf)

// search a consultation by type : for specific doctor
router.route("/doctor-self/search/:type")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    .get(controler.getAllByTypeForDoctor)

// search a consultation by type : specific doctor + specifc patient
router.route("/doctor-self/search/:type/patient/:ssnp")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    .get(controler.getAllByTypeAndPatientForDoctor)

router.route("/patient-self")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    // get all consultations of a specific patient
    .get(controler.getAllByPatient)


router.route("/patient-self/search/:type")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    // get a type of consultation for a specific patient
    .get(controler.getAllByTypeForPatient)

// get one patient consultation for a doctor
router.route("/doctor/patient/:ssnp")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    .get(controler.getOnePatientForDoctor)


router.route("/:id")
    .all((req, res, next) => {
        //fait les premier traitmeent à priorie ici on considère qu'on va dire 200 par défaut
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();

    })
    .get(controler.getOne)
    .delete(controler.deleteOne);


module.exports = router;

