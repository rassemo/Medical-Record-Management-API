function json2htmltab(jsondata,columns)
{
    var table = "<table class='data-table'><tr>";
    for (var k = 0; k < columns.length; k++) { 
        table += "<th>";table += columns[k];table += "</th>";                    
    }
    table += "</tr>"
    for (var i = 0; i < jsondata.length; i++) { 
        table += "<tr>"
        for (var j = 0; j < columns.length; j++) { 
            table += "<td>";
            if(columns[j] == "path"){
                table += "<a target='blanc' href='http://localhost:3000/"+jsondata[i][columns[j]]+"'>&gt&gt</a>";
            }else{
                table += jsondata[i][columns[j]];
            }
            table += "</td>";                    
        }
        table += "</tr>"
    }
    table += "</table>";
    return table;
}
