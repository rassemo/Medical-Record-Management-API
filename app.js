require('./models/db');
require('./middlewares/authentification');


var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require("body-parser");
var logger = require('morgan');
const passport = require('passport');


var indexRouter = require('./routes/index');


// nos routes
var patientRouter = require('./routes/patient.router');
var agentRouter = require('./routes/agent.router');
var analyseRouter = require('./routes/analyse.router');
var radioRouter = require('./routes/radiographie.router');
var consultRouter = require('./routes/consultation.router');
var interventionRouter = require('./routes/intervention.router');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

// autorisation d'acces dossiers
app.use('/analyses',express.static('analyses'));
app.use('/radiographies',express.static('radiographies'));


// mise en place de nos end-points
app.use('/patient', patientRouter);
app.use('/agent', agentRouter);
app.use('/analyse', passport.authenticate('jwt', { session: false }), analyseRouter);
app.use('/radio', passport.authenticate('jwt', { session: false }), radioRouter);
app.use('/consultation', passport.authenticate('jwt', { session: false }), consultRouter);
app.use('/intervention', passport.authenticate('jwt', { session: false }), interventionRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
