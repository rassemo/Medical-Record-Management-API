module.exports = {
    authorize:function(req,roles){
        // role based access
        if (!roles.includes(req.user.role)) {
            throw new Error("Acces denied !")
        }
    }
}