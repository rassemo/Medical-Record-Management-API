const mongoose = require('mongoose');
const schema = mongoose.Schema;

const consultationSchema = new schema(
   {
      ssnPatient: { type: String, required: true },
      ssnAgent: { type: String, required: true },
      // peut etre éviter au vu de la présence de timestamps
      date: { type: Date, required: true },
      // un nom ou remarque associé au fichié
      title: { type: String, required: false },
      // indique le type de consultation
      consultationType: { type: String, enum: ['general', 'ophthalmology', 'gynecologie', 'rheumatology', 'gastrology', 'neuro psychatria', 'surgery', 'dermatology', 'cardiology', 'ORL', 'CCI'], required: true },
      // bref résumé du déroulement de la consultation
      report: { type: String, required: true },
      // 1prescription
      prescription: { type: String, required: false },

   },
   // ajoute date de création et date de Maj
   { collection: "consultation", timestamps: true }

);

module.exports = mongoose.model("consultation", consultationSchema);

