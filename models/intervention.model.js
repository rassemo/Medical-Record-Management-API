const mongoose = require('mongoose');
const schema = mongoose.Schema;

const interventionSchema = new schema(
   {
      ssnPatient: { type: String, required: true },
      ssnAgent: { type: String, required: true },
      // sujet de l'intervention
      title: { type: String, required: true },
      // peut etre éviter au vu de la présence de timestamps
      date: { type: Date, required: true },
      // indique le type de intervention
      interventionType: { type: String, enum: ['ophtalmology', 'gynecology', 'rumathology', 'gastrology', 'neuro-psychatry', 'ORL', 'cermatology', 'cardiology'], required: true },
      // bref résumé du déroulement de la intervention
      report: { type: String, required: true },
      // prescription
      prescription: { type: String, required: false }
   },
   // ajoute date de création et date de Maj
   { collection: "intervention", timestamps: true }

);

module.exports = mongoose.model("intervention", interventionSchema);