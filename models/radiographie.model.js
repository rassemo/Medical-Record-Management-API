const mongoose = require('mongoose');
const schema = mongoose.Schema;

const radiographieSchema = new schema(
   {
      ssnPatient: { type: String, required: true },
      ssnAgent: { type: String, required: true },      // peut etre éviter au vu de la présence de timestamps
      date: { type: Date, required: true },
      // un nom ou remarque associé au fichié
      title: { type: String, required: false },
      // indique le type de radiographie
      radioType: { type: String, enum: ['X-ray', 'echography', 'scanner', 'echo-dopler', 'MRI'], required: true },
      // chemin de fichier
      path: { type: String, required: true },

   },
   // ajoute date de création et date de Maj
   { collection: "radiographie", timestamps: true }

);

module.exports = mongoose.model("Radiographie", radiographieSchema);